#include <algorithm>
#include <array>
#include <vector>
#include <iostream>
#include <fstream>
#include <numeric>
#include <iomanip>
#include <string_view>
#include <ranges>

void day02(std::string_view input_path);

struct Dimensions
{
    int length;
    int width;
    int height;
};

int main(int argc, char *argv[])
{
    if (argc == 0)
    {
        return 0;
    }
    const std::vector<std::string_view> arguments(argv + 1, argv + argc);
    if (arguments.size() != 1)
    {
        std::cout << "usage: [input_file_path]\n";
        return 0;
    }
    day02(arguments[0]);
    return 0;
}

void day02(std::string_view input_path)
{
    std::ifstream input_file(input_path);
    if (!input_file.is_open())
    {
        std::cout << "could not open file " << input_path << '\n';
    }
    std::vector<Dimensions> dimensions_list{};
    for (std::string line{}; std::getline(input_file, line);)
    {
        auto first_delim{line.find('x')};
        auto second_delim{line.find('x', first_delim + 1)};
        Dimensions d{
            std::stoi(line.substr(0, first_delim)),
            std::stoi(line.substr(first_delim + 1, second_delim)),
            std::stoi(line.substr(second_delim + 1, line.size()))};
        dimensions_list.emplace_back(d);
    }

    int total_size = std::transform_reduce(
        dimensions_list.begin(),
        dimensions_list.end(),
        0,
        std::plus<int>{},
        [](Dimensions d)
        {
            auto side1 = d.length * d.width;
            auto side2 = d.width * d.height;
            auto side3 = d.height * d.length;
            return 2 * side1 + 2 * side2 + 2 * side3 + std::min({side1, side2, side3}); });

    std::cout << "total wrapping paper " << total_size << "\n";

    auto total_ribbon = std::transform_reduce(
        dimensions_list.cbegin(),
        dimensions_list.cend(),
        0,
        std::plus<int>{},
        [](Dimensions d)
        {
            std::array<int, 3> sides{d.height, d.width, d.length};
            std::sort(sides.begin(), sides.end());
            auto [min, middle, max] = sides;
            return min + min + middle + middle + (min * middle * max);
        });

    std::cout << "total ribbon " << total_ribbon << "\n";
}
