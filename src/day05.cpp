#include <algorithm>
#include <vector>
#include <array>
#include <fstream>
#include <iostream>
#include <string_view>
#include <string>
#include <regex>

void part1(const std::vector<std::string> &strings);
void part2(const std::vector<std::string> &strings);
bool contains_3_vowels(std::string_view string);
bool contains_2_repeating(std::string_view string);
bool contains_non_overlapping_pair(std::string_view string);
bool repeat_with_letter_between(std::string_view string);

const std::array<char, 5> VOWELS{'a', 'e', 'i', 'o', 'u'};

int main(int argc, char *argv[])
{
    const std::vector<std::string_view> arguments(argv + 1, argv + argc);
    if (arguments.size() != 1)
    {
        std::cout << "usage: [input_file_path]\n";
        return 0;
    }
    std::ifstream input_file(arguments[0]);
    std::vector<std::string> strings{};
    if (input_file.is_open())
    {
        std::string line{};
        while (std::getline(input_file, line))
        {
            strings.push_back(line);
        }
    }
    else
    {
        std::cerr << "could not open file " << arguments[0] << "\n";
        return 1;
    }
    part1(strings);
    part2(strings);
    return 0;
}

void part1(const std::vector<std::string> &strings)
{
    int nice_strings = 0;
    std::regex naughty_regex("ab|cd|pq|xy", std::regex_constants::ECMAScript);
    for (const auto &string : strings)
    {
        if (
            contains_3_vowels(string) && contains_2_repeating(string) && !std::regex_search(string, naughty_regex))
        {
            ++nice_strings;
        }
    }
    std::cout << "total nice strings " << nice_strings << '\n';
}

bool contains_3_vowels(std::string_view string)
{
    int vowel_count = 0;
    for (const auto &letter : string)
    {
        if (std::find(VOWELS.begin(), VOWELS.end(), letter) != VOWELS.end())
        {
            ++vowel_count;
            if (vowel_count == 3)
            {
                return true;
            }
        }
    }
    return false;
}

bool contains_2_repeating(std::string_view string)
{
    char prev{};
    for (const char &letter : string)
    {
        if (letter == prev)
        {
            return true;
        }
        prev = letter;
    }
    return false;
}

void part2(const std::vector<std::string> &strings)
{
    int nice_strings = 0;
    for (const auto &string : strings)
    {
        if (contains_non_overlapping_pair(string) && repeat_with_letter_between(string))
        {
            ++nice_strings;
        }
    }
    std::cout << "total nice strings " << nice_strings << '\n';
}

bool contains_non_overlapping_pair(const std::string_view string)
{
    for (std::size_t i = 0; i < string.size() - 1; ++i)
    {
        auto substr = string.substr(i, 2);
        if (string.find(substr, i + 2) != std::string::npos)
        {
            return true;
        }
    }
    return false;
}

bool repeat_with_letter_between(std::string_view string)
{
    for (std::size_t i = 0; i < string.size() - 2; i++)
    {
        if (string[i] == string[i + 2])
        {
            return true;
        }
    }
    return false;
}
