#include <iostream>
#include <vector>
#include <string_view>
#include <string>
#include <fstream>
#include <regex>
#include <format>
#include <unordered_set>
#include <unordered_map>

struct Operation
{
    enum class Type
    {
        ASSIGN,
        AND,
        OR,
        NOT,
        LSHIFT,
        RSHIFT
    };
    Type type;
    std::string input1, input2, output;
};

bool isdigit(const std::string &string);
void power_circuit(const std::vector<Operation> &operations, std::unordered_map<std::string, std::uint16_t> &outputs);
void topological_sort(std::vector<Operation> &operations);
std::uint16_t solve(std::vector<std::string> &instructions);

int main(int argc, char *argv[])
{
    const std::vector<std::string_view> arguments(argv + 1, argv + argc);
    if (arguments.size() != 1)
    {
        std::cout << "usage: [input_file_path]\n";
        return 0;
    }
    std::ifstream input_file(arguments[0]);
    std::vector<std::string> instruction_strings{};
    if (input_file.is_open())
    {
        std::string line{};
        while (std::getline(input_file, line))
        {
            instruction_strings.push_back(line);
        }
    }
    else
    {
        std::cerr << "could not open file " << arguments[0] << "\n";
        return 1;
    }
    auto part1 = solve(instruction_strings);
    std::cout << "part2 a " << part1 << "\n";
    auto it = std::find_if(
        instruction_strings.begin(),
        instruction_strings.end(),
        [](std::string &s)
        { return s.ends_with("-> b"); });

    std::string new_b_instruction{std::to_string(part1)};
    new_b_instruction.append(" -> b");
    *it = new_b_instruction;
    auto new_a = solve(instruction_strings);
    std::cout << "part2 a " << new_a << "\n";
    return 0;
}

static std::regex assignment_regex{"(\\d+|\\w+) -> (\\w+)"};
static std::regex ops_regex{"(\\d+|\\w+) (\\w+) (\\d+|\\w+) -> (\\w+)"};
static std::regex not_op_regex{"NOT (\\w+) -> (\\w+)"};

std::uint16_t solve(std::vector<std::string> &instructions)
{
    std::vector<Operation> operations{};
    std::smatch match{};
    for (const auto &instruction : instructions)
    {
        if (std::regex_search(instruction, match, ops_regex))
        {
            std::string op{match[2]};
            if (op == "AND")
            {
                operations.push_back(Operation{Operation::Type::AND, match[1], match[3], match[4]});
            }
            else if (op == "OR")
            {
                operations.push_back(Operation{Operation::Type::OR, match[1], match[3], match[4]});
            }
            else if (op == "LSHIFT")
            {
                operations.push_back(Operation{Operation::Type::LSHIFT, match[1], match[3], match[4]});
            }
            else if (op == "RSHIFT")
            {
                operations.push_back(Operation{Operation::Type::RSHIFT, match[1], match[3], match[4]});
            }
        }
        else if (std::regex_search(instruction, match, not_op_regex))
        {
            operations.push_back(Operation{Operation::Type::NOT, match[1], "", match[2]});
        }
        else if (std::regex_search(instruction, match, assignment_regex))
        {
            operations.push_back(Operation{Operation::Type::ASSIGN, match[1], "", match[2]});
        }
        else
        {
            std::cout << instruction << "\n";
            throw std::runtime_error("invalid instruction found");
        }
    }

    topological_sort(operations);
    std::unordered_map<std::string, std::uint16_t> outputs{};
    power_circuit(operations, outputs);
    return outputs.at("a");
}

void topological_sort(std::vector<Operation> &operations)
{
    // assignment statements are the starting nodes.
    // partition them to the front of the list
    // std::cout << "starting topological sort\n";
    // std::cout << "before partition " << operations << '\n';
    auto partition_point = std::partition(
        operations.begin(),
        operations.end(),
        [](auto &operation)
        { return operation.type == Operation::Type::ASSIGN && isdigit(operation.input1); });

    std::unordered_set<std::string> known_wires{};
    for (auto it = operations.begin(); it != partition_point; ++it)
    {
        known_wires.insert(it->output);
    }

    while (partition_point != operations.end())
    {
        for (auto it = partition_point; it != operations.end(); ++it)
        {
            switch (it->type)
            {
            case Operation::Type::AND:
            case Operation::Type::OR:
            case Operation::Type::LSHIFT:
            case Operation::Type::RSHIFT:
                if ((isdigit(it->input1) || known_wires.contains(it->input1)) && (isdigit(it->input2) || known_wires.contains(it->input2)))
                {
                    known_wires.insert(it->output);
                    if (it != partition_point)
                        std::iter_swap(partition_point, it);

                    ++partition_point;
                }
                break;

            case Operation::Type::NOT:
                if (isdigit(it->input1) || known_wires.contains(it->input1))
                {
                    known_wires.insert(it->output);
                    if (it != partition_point)
                        std::iter_swap(partition_point, it);

                    ++partition_point;
                }
                break;

            case Operation::Type::ASSIGN:
                if (known_wires.contains(it->input1))
                {
                    known_wires.insert(it->output);
                    if (it != partition_point)
                        std::iter_swap(partition_point, it);

                    ++partition_point;
                }
                break;
            }
        }
    }
}

void power_circuit(const std::vector<Operation> &operations, std::unordered_map<std::string, std::uint16_t> &outputs)
{
    std::uint16_t left_value{}, right_value{};
    for (auto &op : operations)
    {
        switch (op.type)
        {
        case Operation::Type::ASSIGN:
            if (isdigit(op.input1))
                outputs.insert({op.output, std::stoi(op.input1)});
            else
                outputs.insert({op.output, outputs.at(op.input1)});

            break;

        case Operation::Type::AND:
            left_value = isdigit(op.input1) ? std::stoi(op.input1) : outputs.at(op.input1);
            right_value = isdigit(op.input2) ? std::stoi(op.input2) : outputs.at(op.input2);
            outputs.insert({op.output, left_value & right_value});
            break;

        case Operation::Type::OR:
            left_value = isdigit(op.input1) ? std::stoi(op.input1) : outputs.at(op.input1);
            right_value = isdigit(op.input2) ? std::stoi(op.input2) : outputs.at(op.input2);
            outputs.insert({op.output, left_value | right_value});
            break;

        case Operation::Type::LSHIFT:
            left_value = isdigit(op.input1) ? std::stoi(op.input1) : outputs.at(op.input1);
            right_value = isdigit(op.input2) ? std::stoi(op.input2) : outputs.at(op.input2);
            outputs.insert({op.output, left_value << right_value});
            break;

        case Operation::Type::RSHIFT:
            left_value = isdigit(op.input1) ? std::stoi(op.input1) : outputs.at(op.input1);
            right_value = isdigit(op.input2) ? std::stoi(op.input2) : outputs.at(op.input2);
            outputs.insert({op.output, left_value >> right_value});
            break;

        case Operation::Type::NOT:
            left_value = isdigit(op.input1) ? std::stoi(op.input1) : outputs.at(op.input1);
            outputs.insert({op.output, ~left_value});
            break;
        }
    }
}

bool isdigit(const std::string &string)
{
    return string.size() > 0 && std::all_of(string.begin(), string.end(), [](char c)
                                            { return std::isdigit(c); });
}
