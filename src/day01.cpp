#include <fstream>
#include <iostream>
#include <optional>
#include <string>
#include <string_view>
#include <vector>

void day01(std::string_view input_path);

int main(int argc, char *argv[])
{
    const std::vector<std::string_view> arguments(argv + 1, argv + argc);
    if (arguments.size() != 1)
    {
        std::cout << "usage: [input_file_path]\n";
        return 0;
    }
    day01(arguments.at(0));
    return 0;
}

void day01(std::string_view input_path)
{
    int current_floor{0}, position{0};
    std::optional<int> basement_step{};
    std::string line{};
    std::ifstream input_file(input_path);
    if (input_file.is_open())
    {
        std::getline(input_file, line);
    }
    else
    {
        std::cerr << "could not open file " << input_path << "\n";
        return;
    }
    for (const auto &step : line)
    {
        position++;
        if (step == '(')
            current_floor++;

        else if (step == ')')
            current_floor--;

        if (basement_step == std::nullopt && current_floor == -1)
            basement_step = position;
    }
    std::cout << "final floor: " << current_floor << '\n';
    std::cout << "entered basement on " << basement_step.value() << '\n';
}
