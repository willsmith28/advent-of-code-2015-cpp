#include <vector>
#include <string>
#include <string_view>
#include <iostream>
#include <fstream>
#include <unordered_set>

void part1(std::string_view directions);
void part2(std::string_view directions);

template <class T>
inline void hash_combine(std::size_t &seed, const T &v)
{
    // boost magic
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

struct Position
{
    int x, y;

    Position(int x_, int y_) : x{x_}, y{y_} {}

    bool operator==(const Position &rhs) const
    {
        return x == rhs.x && y == rhs.y;
    }
    struct Hash
    {
        std::size_t operator()(const Position &p) const
        {
            std::size_t result{0};
            hash_combine(result, p.x);
            hash_combine(result, p.y);
            return result;
        }
    };
};

int main(int argc, char *argv[])
{
    const std::vector<std::string_view> arguments(argv + 1, argv + argc);
    if (arguments.size() != 1)
    {
        std::cout << "usage: [input_file_path]\n";
        return 0;
    }
    std::string line{};
    std::ifstream input_file(arguments[0]);
    if (input_file.is_open())
    {
        std::getline(input_file, line);
    }
    else
    {
        std::cerr << "could not open file " << arguments[0] << "\n";
        return 1;
    }
    part1(line);
    part2(line);
    return 0;
}

void part1(std::string_view directions)
{
    Position current_location{0, 0};
    std::unordered_set<Position, Position::Hash> visited_locations{{0, 0}};
    for (const char &direction : directions)
    {
        switch (direction)
        {
        case '^':
            ++current_location.y;
            break;

        case 'v':
            --current_location.y;
            break;

        case '>':
            ++current_location.x;
            break;

        case '<':
            --current_location.x;
            break;

        default:
            std::cerr << "invalid character present in input file";
            break;
        }
        visited_locations.emplace(current_location.x, current_location.y);
    }
    std::cout << "positions visited at least once " << visited_locations.size() << '\n';
}

void part2(std::string_view directions)
{
    Position santas_location{0, 0};
    Position robo_santas_location{0, 0};
    std::unordered_set<Position, Position::Hash> visited_locations{{0, 0}};
    int step{1};
    Position *current_location;
    for (const char &direction : directions)
    {
        current_location = (step % 2 == 1) ? &santas_location : &robo_santas_location;
        switch (direction)
        {
        case '^':
            ++current_location->y;
            break;

        case 'v':
            --current_location->y;
            break;

        case '>':
            ++current_location->x;
            break;

        case '<':
            --current_location->x;
            break;

        default:
            std::cerr << "invalid character present in input file";
            break;
        }

        visited_locations.insert(*current_location);
        ++step;
    }
    std::cout << "with 2 santas positions visited at least once " << visited_locations.size() << '\n';
}
