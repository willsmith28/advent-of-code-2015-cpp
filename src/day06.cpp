#include <vector>
#include <array>
#include <fstream>
#include <iostream>
#include <string_view>
#include <string>
#include <regex>
#include <iterator>

struct Coord
{
    std::size_t x, y;
};

struct Instruction
{
    enum class Action
    {
        on,
        off,
        toggle,
    };

    Action action;
    Coord begin, end;
};

void parse_instructions(const std::vector<std::string> &instruction_strings, std::vector<Instruction> &instructions);
void part1(const std::vector<Instruction> &instructions);
void part2(const std::vector<Instruction> &instructions);

class Grid
{
    std::array<std::array<bool, 1000>, 1000> m_grid{};

public:
    Grid() : m_grid{} {}

    void follow_instruction(const Instruction &instruction)
    {
        for (std::size_t y = instruction.begin.y; y <= instruction.end.y; ++y)
        {
            for (std::size_t x = instruction.begin.x; x <= instruction.end.x; ++x)
            {
                switch (instruction.action)
                {
                case Instruction::Action::on:
                    m_grid[y][x] = true;
                    break;

                case Instruction::Action::off:
                    m_grid[y][x] = false;
                    break;

                case Instruction::Action::toggle:
                    m_grid[y][x] = !m_grid[y][x];
                    break;
                }
            }
        }
    }

    int lit_count()
    {
        int total{0};
        for (const auto &row : m_grid)
        {
            for (const auto &lit : row)
            {
                if (lit)
                    ++total;
            }
        }
        return total;
    }

    void print_grid()
    {

        for (const auto &row : m_grid)
        {
            for (const auto &lit : row)
            {
                std::cout << lit;
            }
            std::cout << '\n';
        }
    }
};

class BrightnessGrid
{
    std::array<std::array<int, 1000>, 1000> m_grid{};

public:
    BrightnessGrid() : m_grid{} {}

    void follow_instruction(const Instruction &instruction)
    {
        for (std::size_t y = instruction.begin.y; y <= instruction.end.y; ++y)
        {
            for (std::size_t x = instruction.begin.x; x <= instruction.end.x; ++x)
            {
                switch (instruction.action)
                {
                case Instruction::Action::on:
                    ++m_grid[y][x];
                    break;

                case Instruction::Action::off:
                    if (m_grid[y][x] > 0)
                        --m_grid[y][x];

                    break;

                case Instruction::Action::toggle:
                    m_grid[y][x] += 2;
                    break;
                }
            }
        }
    }

    int total_brightness()
    {
        int total{0};
        for (const auto &row : m_grid)
        {
            for (const auto &brightnes : row)
            {
                total += brightnes;
            }
        }
        return total;
    }
};

int main(int argc, char *argv[])
{
    const std::vector<std::string_view> arguments(argv + 1, argv + argc);
    if (arguments.size() != 1)
    {
        std::cout << "usage: [input_file_path]\n";
        return 0;
    }
    std::ifstream input_file(arguments[0]);
    std::vector<std::string> instruction_strings{};
    if (input_file.is_open())
    {
        std::string line{};
        while (std::getline(input_file, line))
        {
            instruction_strings.push_back(line);
        }
    }
    else
    {
        std::cerr << "could not open file " << arguments[0] << "\n";
        return 1;
    }

    std::vector<Instruction> instructions{};
    parse_instructions(instruction_strings, instructions);
    part1(instructions);
    part2(instructions);
    return 0;
}

void part1(const std::vector<Instruction> &instructions)
{
    Grid grid{};
    for (const auto &instruction : instructions)
    {
        grid.follow_instruction(instruction);
    }
    std::cout << "total lit " << grid.lit_count() << '\n';
}

void part2(const std::vector<Instruction> &instructions)
{
    BrightnessGrid grid{};
    for (const auto &instruction : instructions)
    {
        grid.follow_instruction(instruction);
    }
    std::cout << "total brightness " << grid.total_brightness() << '\n';
}

void parse_instructions(const std::vector<std::string> &instruction_strings, std::vector<Instruction> &instructions)
{
    std::regex regex("(.*) (\\d+,\\d+) through (\\d+,\\d+)", std::regex_constants::ECMAScript);
    std::transform(
        instruction_strings.begin(),
        instruction_strings.end(),
        std::back_inserter(instructions),
        [regex](const std::string &instruction_string)
        {
            std::smatch match;
            if (!std::regex_search(instruction_string, match, regex))
            {
                std::cerr << "invalid input string in file: " << instruction_string << '\n';
                throw std::runtime_error("illegal instruction format");
            }

            Instruction::Action action{};
            if (match[1] == "turn on")
                action = Instruction::Action::on;

            else if (match[1] == "turn off")
                action = Instruction::Action::off;

            else if (match[1] == "toggle")
                action = Instruction::Action::toggle;

            else
                throw std::runtime_error("illegal instruction format");

            std::string string = match[2].str();
            auto delim{string.find(',')};
            Coord begin{
                std::stoul(string.substr(0, delim)),
                std::stoul(string.substr(delim + 1, string.size()))};

            string = match[3].str();
            delim = string.find(',');
            Coord end{
                std::stoul(string.substr(0, delim)),
                std::stoul(string.substr(delim + 1, string.size()))};

            return Instruction{action, begin, end};
        });
}
