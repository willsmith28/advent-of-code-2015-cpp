mkdir -p build/
exec clang++ -std=c++20 -Weverything -Wno-c++98-compat -Wno-c++98-compat-pedantic -o build/$1 src/$1.cpp
